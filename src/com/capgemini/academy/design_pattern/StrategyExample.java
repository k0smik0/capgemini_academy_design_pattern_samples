package com.capgemini.academy.design_pattern;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.capgemini.academy.design_pattern.StrategyExample.ContextUsingMapEnumFunctor.Type;

/**
 * @author massimiliano.leone@capgemini.com
 *
 *         29 Oct 2021
 *
 */
public class StrategyExample {

	public interface DocumentExporter {
		String export();
	}

	// @Component
	public static class PDFDocumentExporter implements DocumentExporter {
		// we could injecting some dep, but careful, u need @Component on top of class declaration
		// @Autowired MyBla bla;
		@Override
		public String export() {
			return "PDF";
		}
	}

	public static class ODTDocumentExporter implements DocumentExporter {
		@Override
		public String export() {
			return "ODT";
		}
	}

	public static class DOCXDocumentExporter implements DocumentExporter {
		@Override
		public String export() {
			return "DOCX";
		}
	}

	public static class ContextUsingIfElsePrototypal {
		public String export(String param) {
			if (param.equalsIgnoreCase("PDF")) {
				return new PDFDocumentExporter().export();
			} else if (param.equalsIgnoreCase("ODT")) {
				return new ODTDocumentExporter().export();
			} else if (param.equalsIgnoreCase("DOCX")) {
				return new DOCXDocumentExporter().export();
			} else {
				return "wrong param";
			}
		}
	}

	public static abstract class AbstractContextUsingInstanceFields {
		protected static final PDFDocumentExporter pdfDocumentExporter = new PDFDocumentExporter();
		/*-
		 *  or injecting via container
		 *  @Autowired PDFDocumentExporter pdfDocumentExporter
		 */
		protected static final ODTDocumentExporter odtDocumentExporter = new ODTDocumentExporter();
		protected static final DOCXDocumentExporter docxDocumentExporter = new DOCXDocumentExporter();
	}

	public static class ContextUsingIfElseWithInstanceFields extends AbstractContextUsingInstanceFields {
		public String export(String param) {
			if (param.equalsIgnoreCase("PDF")) {
				return pdfDocumentExporter.export();
			} else if (param.equalsIgnoreCase("ODT")) {
				return odtDocumentExporter.export();
			} else if (param.equalsIgnoreCase("DOCX")) {
				return docxDocumentExporter.export();
			} else {
				return "wrong param";
			}
		}
	}

	public static class ContextUsingMapFunctor extends AbstractContextUsingInstanceFields {
		// using map as functor: a pointer of functions
		private final Map<String, DocumentExporter> exportersMap = new HashMap<>();

		public ContextUsingMapFunctor() {
			initMap();
		}

		/*-
			if we use dependency injection (@Autowired/@Inject) on fields
			PDFDocumentExporter, DOCXDocumentExporter, etc...
		 	those deps are initialized AFTER constructor calls, so that
		 	initMap above could be fail; so we could use @PostConstruct
		 	annotated method: this is a "constructor" in dependency injection
		 	world; also, it is recommended to NOT use injection on fields,
		 	but on class constructor (if allowed)
		 */
		/*-
		 @PostConstruct
		 private void postInit() {
		 	initMap();
		 }
		 */

		// or also directly into constructor
		private void initMap() {
			// careful! these instances below must be initialized before to put into map
			// instance into constructor before init map or declare inline as in abstract class
			exportersMap.put("PDF", pdfDocumentExporter);
			exportersMap.put("ODT", odtDocumentExporter);
			exportersMap.put("DOCX", docxDocumentExporter);
		}

		public String export(String param) {
			// NullPointerException if param is null !
			// exportersMap.get(param).export();

			// old way check
			/*-
			DocumentExporter documentExporter = exportersMap.get(param);
			if (documentExporter != null) {
				return documentExporter.export();
			} else {
				return "wrong param";
			}
			*/

			// java >= 8 way, using anonymous class
			/*-
			return Optional.ofNullable( exportersMap.get(param) )
				.map(new Function<DocumentExporter, String>() {
					@Override
					public String apply(DocumentExporter t) {
						return t.export();
					}
				})
				.orElse("wrong param");
			*/

			// java 8 lambda
			/*-
			return Optional.ofNullable(exportersMap.get(param))
				.map(t -> t.export())
				.orElse("wrong param");
			*/

			// java 8 method reference, the best way, because
			// method reference has better performance than lambda:
			// so use it if possible
			return Optional.ofNullable(exportersMap.get(param))
				.map(DocumentExporter::export)
				.orElse("wrong param");
		}
	}

	public static class ContextUsingMapEnumFunctor extends AbstractContextUsingInstanceFields {
		// using EnumMap as functor: EnumMap use Enum as keys, so it provides strictly check on values
		private final Map<Type, DocumentExporter> exportersEnumMap = new EnumMap<>(Type.class);

		public ContextUsingMapEnumFunctor() {
			initEnumMap();
		}

		public enum Type {
			PDF,
			ODT,
			DOCX;
		}

		private void initEnumMap() {
			exportersEnumMap.put(Type.PDF, pdfDocumentExporter);
			exportersEnumMap.put(Type.ODT, odtDocumentExporter);
			exportersEnumMap.put(Type.DOCX, docxDocumentExporter);
		}

		// no need to check type for values, because the strong enum Type
		public String export(Type param) {
			// but hey! we could have again 'null' in param, so we should use Option
			// return exportersEnumMap.get(param).export();
			return Optional.ofNullable(exportersEnumMap.get(param))
				.map(DocumentExporter::export)
				.orElse("wrong param");
		}
	}

	// using pure Enum as functor - it is allocated at (almost) jvm boot time
	// no dependency injection allowed here
	public enum ContextUsingEnum implements DocumentExporter {
		PDF {
			@Override
			public String export() {
				used = true;
				return "PDF";
			}
		},
		ODT {
			@Override
			public String export() {
				return "ODT";
			}
		},
		DOCX {
			@Override
			public String export() {
				used = true;
				return "DOCX";
			}
		};

		protected boolean used;

		public boolean getUsed() {
			return used;
		}
	}

	public static void main(String[] args) {
		System.out.println(new ContextUsingIfElsePrototypal().export("PDF"));
		System.out.println(new ContextUsingIfElseWithInstanceFields().export("PDF"));
		System.out.println(new ContextUsingMapFunctor().export("PDF"));

		ContextUsingMapEnumFunctor contextUsingMapEnumFunctor = new ContextUsingMapEnumFunctor();
		System.out.println(contextUsingMapEnumFunctor.export(Type.PDF));
		// try to discover matching type
		try {
			Type valueOf = Type.valueOf(args[0]);
			// here args[0] is one of enumerated
			System.out.println(contextUsingMapEnumFunctor.export(valueOf));
		} catch (IllegalArgumentException e) {
			// here it is not, so catch
		}

		System.out.println(ContextUsingEnum.PDF.export());
		// same as above, but acting directly on Enum
		try {
			// strategy using enum
			ContextUsingEnum ee = ContextUsingEnum.valueOf(args[0]);
			ee.export();
			ee.getUsed();
		} catch (IllegalArgumentException e) {
		}

	}

}
