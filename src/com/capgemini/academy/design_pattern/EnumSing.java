package com.capgemini.academy.design_pattern;

/**
 * @author massimiliano.leone@capgemini.com
 *
 *         27 Oct 2021
 *
 */
public enum EnumSing {

	INSTANCE;

	private boolean setup;
	private String a;

	public EnumSing init(String a) {
		if (!setup) {
			this.a = a;
		}
		return this;
	}

}
