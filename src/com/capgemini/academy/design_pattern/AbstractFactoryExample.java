package com.capgemini.academy.design_pattern;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author massimiliano.leone@capgemini.com
 *
 *         31 Oct 2021
 *
 */
public class AbstractFactoryExample {

	public interface WonderfulCar {
		Engine getEngine();

		Model getModel();

		String getDescription();
	}

	// a commodity abstract static class for common field/constructor
	public abstract static class CarElement {
		protected final String name;

		CarElement(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	public static class Engine extends CarElement {
		public Engine(String name) {
			super(name);
		}
	}

	private static class Model extends CarElement {
		public Model(String name) {
			super(name);
		}
	}

	public static abstract class UnknownWonderfulCar implements WonderfulCar {
		private final Engine engine;
		private final Model model;

		UnknownWonderfulCar(Engine engine, Model model) {
			this.engine = engine;
			this.model = model;
		}

		@Override
		public Engine getEngine() {
			return engine;
		}

		@Override
		public Model getModel() {
			return model;
		}

		@Override
		public String getDescription() {
			return model.getName() + " with " + engine.getName();
		}
	}

	public static class Ferrari extends UnknownWonderfulCar {
		public Ferrari(Engine engine, Model model) {
			super(engine, model);
		}
	}

	public static class Dodge extends UnknownWonderfulCar {
		public Dodge(Engine engine, Model model) {
			super(engine, model);
		}
	}

	public static class Lamborghini extends UnknownWonderfulCar {
		public Lamborghini(Engine engine, Model model) {
			super(engine, model);
		}
	}

	public interface WonderfulCarFactory {
		Model makeModel();

		Engine makeEngine();
	}

	public static class FerrariFactory implements WonderfulCarFactory {
		@Override
		public Model makeModel() {
			return new Model("Ferrari Testarossa");
		}

		@Override
		public Engine makeEngine() {
			return new Engine("180° V12 DOHC");
		}
	}

	public static class DodgeFactory implements WonderfulCarFactory {
		@Override
		public Model makeModel() {
			return new Model("Dodge Viper");
		}

		@Override
		public Engine makeEngine() {
			return new Engine("Naturally-aspirated 90° V10");
		}
	}

	public static class LamborghiniFactory implements WonderfulCarFactory {
		@Override
		public Model makeModel() {
			return new Model("Lamborghini Diablo");
		}

		@Override
		public Engine makeEngine() {
			return new Engine("6.0 L V12");
		}
	}

	enum CarBuilder {
		FERRARI(new FerrariFactory()),
		LAMBORGHINI(new LamborghiniFactory()),
		DODGE(new DodgeFactory());

		private final WonderfulCarFactory factory;

		private CarBuilder(WonderfulCarFactory factory) {
			this.factory = factory;
		}

		public WonderfulCar makeCar() {
			return new UnknownWonderfulCar(factory.makeEngine(), factory.makeModel()) {
			};
		}

		public static List<WonderfulCar> makeCars() {
			return Arrays.stream(CarBuilder.values()).map(CarBuilder::makeCar).collect(Collectors.toList());
		}
	}

	public static void main(String[] args) {
		// generate a list of WonderfulCar using java8 stream and method reference
		List<WonderfulCar> cars = CarBuilder.makeCars();
		// iterate on stream and do something
		cars.forEach(wc -> System.out.println(wc.getDescription()));

		/*-
		 * OUTPUT WILL BE:
		 *
		 	-----
			Testarossa with 180° V12 DOHC
			Diablo with 6.0 L V12
			Dodge Viper with Naturally-aspirated 90° V10
			-----
		 *
		 */
	}

	/*-
	 *
	 * Spring way
	 *
	 *
	 */

	@Configuration
	public static class SpringWonderfulCarFactory {

		@Qualifier("FerrariModel")
		@Bean
		public Model makeFerrariModel() {
			return new Model("Ferrari Testarossa");
		}

		@Qualifier("FerrariEngine")
		@Bean
		public Engine makeFerrariEngine() {
			return new Engine("180° V12 DOHC");
		}

		@Qualifier("Ferrari")
		@Bean
		public WonderfulCar makeFerrari(@Qualifier("FerrariModel") Model ferrariModel, @Qualifier("FerrariEngine") Engine ferrariEngine) {
			return makeWonderfulCar(ferrariModel, ferrariEngine);
		}

		@Qualifier("LamborghiniModel")
		@Bean
		public Model makeLamborghiniModel() {
			return new Model("Lamborghini Diablo");
		}

		@Qualifier("LamborghiniEngine")
		@Bean
		public Engine makeLamborghiniEngine() {
			return new Engine("6.0 L V12");
		}

		@Qualifier("Lamborghini")
		@Bean
		public WonderfulCar makeLamborghini(@Qualifier("LamborghiniModel") Model lamborghiniModel, @Qualifier("LamborghiniEngine") Engine lamborghiniEngine) {
			return makeWonderfulCar(lamborghiniModel, lamborghiniEngine);
		}

		private WonderfulCar makeWonderfulCar(Model model, Engine engine) {
			return new UnknownWonderfulCar(engine, model) {
			};
		}
	}

	@Component
	public static class SpringCarBuilder {
		private final List<WonderfulCar> cars;

		@Autowired
		public SpringCarBuilder(@Qualifier("Ferrari") WonderfulCar ferrari, @Qualifier("Lamborghini") WonderfulCar lamborghini) {
			this.cars = List.of(ferrari, lamborghini);
		}

		public static void main(String[] args) {
			AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext();
			ac.register(SpringWonderfulCarFactory.class);
			ac.register(SpringCarBuilder.class);
			ac.refresh();
			SpringCarBuilder springCarBuilder = ac.getBean(SpringCarBuilder.class);
			springCarBuilder.cars.forEach(wc -> System.out.println(wc.getDescription()));
		}

		/*
		 * // spring boot itself use Factory for its purposes
		 *
		 * @Bean
		 * CommandLineRunner commandLineRunner(@SuppressWarnings("unused") final String... args) {
		 * return a -> {
		 * System.out.println(ferrari.getDescription());
		 * };
		 * }
		 */
	}

}
