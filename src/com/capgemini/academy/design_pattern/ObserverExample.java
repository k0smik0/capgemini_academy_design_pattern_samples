package com.capgemini.academy.design_pattern;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author massimiliano.leone@capgemini.com
 *
 *         29 Oct 2021
 *
 */
public class ObserverExample {

	// extending Comparable we could have all observers sorted by TreeSet (see below)
	public interface MyObserver extends Comparable<MyObserver> {
		void update(String event);

		String getName();
	}

	public static class AbstractMyObserver implements MyObserver {
		protected final String name;

		public AbstractMyObserver(String name) {
			this.name = name;
		}

		@Override
		public void update(String event) {
			System.out.println("Received event: " + event);
		}

		@Override
		public int compareTo(MyObserver o) {
			return this.getName().compareTo(o.getName());
		}

		@Override
		public String getName() {
			return name;
		}
	}

	public static class MyObserver1 extends AbstractMyObserver {
		public MyObserver1() {
			super("MyObserver1");
		}
	}

	public static class MyObserver2 extends AbstractMyObserver {
		public MyObserver2() {
			super("MyObserver2");
		}
	}

	static class EventSource {

		// using list - looping on list respect order insertion
//		private final List<MyObserver> myObservers = new ArrayList<>();

		// using hashset - looping on hashset respect its internal order
//		private final Set<MyObserver> MyObservers = new HashSet<>();
		// using treeset - finally we could have our custom order, but MyObserver hierarchy must implement Comparable
		private final Set<MyObserver> myObservers = new TreeSet<>();

		private void notifyMyObservers(String event) {
			myObservers.forEach(MyObserver -> MyObserver.update(event));
		}

		public void addMyObserver(MyObserver MyObserver) {
			myObservers.add(MyObserver);
		}

		// a dummy event generator
		public void generateEvents() {
			List<String> events = List.of("eventA", "eventB", "eventC");
			Iterator<String> iterator = events.iterator();
			while (iterator.hasNext()) {
				String line = iterator.next();
				notifyMyObservers(line);
			}
		}
	}

	public static void main(String[] args) {
		EventSource eventSource = new EventSource();

		MyObserver myObserver1 = new MyObserver1();
		MyObserver myObserver2 = new MyObserver2();
		// register known observers
		eventSource.addMyObserver(myObserver2);
		eventSource.addMyObserver(myObserver1);

		// register anonymous observer
		eventSource.addMyObserver(new MyObserver() {
			@Override
			public void update(String event) {
				System.out.println("Received event: " + event);
			}

			@Override
			public int compareTo(MyObserver o) {
				return this.getName().compareTo(o.getName());
			}

			@Override
			public String getName() {
				return "MyObserverAnonymous";
			}
		});

		System.out.println("new events...");
		eventSource.generateEvents();
	}

}
