package com.capgemini.academy.design_pattern;

import java.util.function.Consumer;

import com.capgemini.conteggi.utils.acceptor.Acceptor;
import com.capgemini.conteggi.utils.acceptor.Acceptors;

/**
 * @author massimiliano.leone@capgemini.com
 *
 *         2 Nov 2021
 *
 */
public class AcceptorVisitorExample {

	public static class ConcreteA {
	}

	public static class ConcreteB {
	}

	public static void doSomethingOnA(ConcreteA concreteA) {
		System.out.println(concreteA);
	}

	public static void main(String[] args) {

		ConcreteA concreteA = new ConcreteA();
		ConcreteB concreteB = new ConcreteB();
		// method doSomethingXXX inside Consumer are locally implemented, or externally referenced via closure
		Consumer<ConcreteA> aConsumerDoingVisitorUsingLambda = a -> {
			doSomethingOnA(a);
		};
		Consumer<ConcreteA> aConsumerDoingVisitorUsingMethodReference = AcceptorVisitorExample::doSomethingOnA;
		Consumer<ConcreteB> bConsumerDoingVisitor = System.out::println;

		Acceptor<ConcreteA> acceptorConcreteA = Acceptors.fromObject(concreteA);
		acceptorConcreteA.accept(aConsumerDoingVisitorUsingLambda);
		acceptorConcreteA.accept(aConsumerDoingVisitorUsingMethodReference);
		Acceptor<ConcreteB> acceptorConcreteB = Acceptors.fromObject(concreteB);
		acceptorConcreteB.accept(bConsumerDoingVisitor);
	}

}
