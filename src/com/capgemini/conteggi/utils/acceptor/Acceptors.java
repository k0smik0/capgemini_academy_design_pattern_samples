package com.capgemini.conteggi.utils.acceptor;

/*
 * from https://github.com/cbojar/acceptor-java
 */
public final class Acceptors {
	public static <T> Acceptor<T> fromObject(final T object) {
		return new ObjectAcceptor<>(object);
	}

	public static <T> Acceptor<T> fromIterable(final Iterable<T> iterable) {
		return new IterableAcceptor<>(iterable);
	}

	private Acceptors() {}
}
