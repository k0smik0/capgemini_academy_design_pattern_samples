package com.capgemini.conteggi.utils.acceptor;

import java.util.function.Consumer;

/*
 * from https://github.com/cbojar/acceptor-java
 */
public interface Acceptor<T> {
	Acceptor<T> accept(Consumer<T> visitor);

	<U extends T> Acceptor<T> accept(Class<U> klass, Consumer<? super U> visitor);
}
