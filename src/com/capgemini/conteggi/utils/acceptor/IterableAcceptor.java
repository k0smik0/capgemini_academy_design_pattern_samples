package com.capgemini.conteggi.utils.acceptor;

import java.util.function.Consumer;

/*
 * from https://github.com/cbojar/acceptor-java
 */
public class IterableAcceptor<T> implements Acceptor<T> {
	private final Iterable<T> values;

	public IterableAcceptor(final Iterable<T> values) {
		this.values = values;
	}

	@Override
	public Acceptor<T> accept(final Consumer<T> visitor) {
		for (T value : values) {
			new ObjectAcceptor<>(value).accept(visitor);
		}
		return this;
	}

	@Override
	public <U extends T> Acceptor<T> accept(final Class<U> klass, final Consumer<? super U> visitor) {
		for (T value : values) {
			new ObjectAcceptor<>(value).accept(klass, visitor);
		}
		return this;
	}
}
