package com.capgemini.conteggi.utils.acceptor;

import java.util.function.Consumer;

/*
 * from https://github.com/cbojar/acceptor-java
 */
public class ObjectAcceptor<T> implements Acceptor<T> {
	private final T value;

	public ObjectAcceptor(final T value) {
		this.value = value;
	}

	@Override
	public Acceptor<T> accept(final Consumer<T> visitor) {
		visitor.accept(value);
		return this;
	}

	@Override
	public <U extends T> Acceptor<T> accept(final Class<U> klass, final Consumer<? super U> visitor) {
		if (isMyType(klass)) {
			visitor.accept(klass.cast(value));
		}
		return this;
	}

	private <U extends T> boolean isMyType(final Class<U> klass) {
		return klass.isAssignableFrom(value.getClass());
	}
}
